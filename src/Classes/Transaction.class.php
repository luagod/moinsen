<?php

namespace Michy2k\CoinpaymentsBundle\Classes;

class Transaction
{
    /**
     * @var string
     */
    private $secretKey;

    /**
     * @var int
     */
    private $merchantId;

    /**
     * @var bool
     */
    private $isHttpAuth;

    /**
     * Set secret key
     *
     * @param string $secretKey
     */
    public function setSecretKey($secretKey) {
        $this->secretKey = $secretKey;
    }

    /**
     * Get secret key
     *
     * @return string
     */
    public function getSecretKey() {
        return $this->secretKey;
    }

    /**
     * Set merchant id
     *
     * @param string
     */
    public function setMerchantId($merchantId) {
        $this->merchantId = $merchantId;
    }

    /**
     * Get merchant id
     *
     * @return int
     */
    public function getMerchantId() {
        return $this->merchantId;
    }

    /**
     * Set isHttpAutch
     *
     * @param bool $isHttpAuth
     */
    public function setIsHttpAuth($isHttpAuth) {
        $this->isHttpAuth = $isHttpAuth;
    }

    /**
     * Get isHttpAuth
     *
     * @return bool
     */
    public function getIsHttpAuth() {
        return $this->isHttpAuth;
    }
}
