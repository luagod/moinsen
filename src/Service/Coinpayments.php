<?php

namespace Michy2k\CoinpaymentsBundle\Service;

use Michy2k\CoinpaymentsBundle\Classes\Transaction;
use Symfony\Component\DependencyInjection\ContainerInterface;

class Coinpayments
{

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Constructor method
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    /**
     * Create transaction
     *
     * @param string $secretKey
     * @param int $merchantId
     * @param bool $isHttpAuth
     * @return Transaction|null
     */
    public function createTransaction($secretKey, $merchantId, $isHttpAuth) {

        if (empty($secretKey)) {
            $secretKey = $this->container->getParameter('coinpayments.secret_key');
        }

        if (empty($merchantId)) {
            $merchantId = $this->container->getParameter('coinpayments.merchant_id');
        }

        if (empty($isHttpAuth)) {
            $isHttpAuth = $this->container->getParameter('coinpayments.is_http_auth');
        }

        if (
            empty($secretKey) ||
            empty($merchantId) ||
            empty($isHttpAuth)
        ) {
            return null;
        }

        $transaction = new Transaction();
        $transaction->setSecretKey($secretKey);
        $transaction->setMerchantId($merchantId);
        $transaction->setIsHttpAuth($isHttpAuth);

        return $transaction;
    }
}
